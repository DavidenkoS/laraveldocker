# change owner of dir with app 
sudo chown -R $USER:$USER $(pwd)

# to run compose
docker-compose up -d

#docker compose exec for invoke .env (will be replaced)
docker-compose exec app php artisan key:generate

docker-compose exec app php artisan config:cache

#TODO replace docker compose exec with vars form bash env of container 
